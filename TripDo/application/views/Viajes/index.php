<div class="conteiner" style="display: flex;flex-direction: row;justify-content: space-between;">
<div class="card" style="width: 80%;margin: 20px;border: none">
<div class="card text-center" style="border: none">
<div class="card-body" style="display: flex;justify-content: space-between;">
<i class="fas fa-long-arrow-alt-left" style="font-size: 30px;"></i>
<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#exampleModal">
NUEVO
</button>

  </div>

  <div class="card-body" style="    height: 150px;">
    <h5 class="card-title" style="    font-size: 30px;font-weight: 600;text-align: start;">Sale viaje con los pibes</h5>

  </div>
</div>
<div class="card" style="border: none">
<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Inicio</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Destinos</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Planes</a>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab" style="display: flex;
    flex-direction: row;">

  <div class="card" style=" width: 400px;margin: 30px;">
    <img class="card-img-top" src="https://aws.revistavanityfair.es/prod/designs/v1/assets/785x589/157946.jpg" alt="Card image cap" style=" height: 250px;">
    <div class="card-body">
      <h5 class="card-title">Card title</h5>
      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
    </div>
  </div>
  <div class="card" style=" width: 400px;margin: 30px;">
    <img class="card-img-top" src="https://okdiario.com/img/2018/03/12/paises-mas-visitados-655x368.jpg" alt="Card image cap" style="height: 250px;">
    <div class="card-body">
      <h5 class="card-title">Card title</h5>
      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
    </div>
  </div>
  <div class="card" style=" width: 400px;margin: 30px;">
    <img class="card-img-top" src="https://www.ambientum.com/wp-content/uploads/2019/11/holanda-amsterdam-696x464.jpg" alt="Card image cap" style="height: 250px;">
    <div class="card-body">
      <h5 class="card-title">Card title</h5>
      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
    </div>
  </div>

  
  </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
</div>


</div>


</div>



<div class="card" style="width: 18rem; margin: 20px;">
<iframe src="https://maps.google.com/maps?q=manhatan&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0"
    style="border:0;width: 100%;height: 300px;margin:0px" allowfullscreen></iframe>
    <div class="card-header">
    Actividad Reciente
  </div>
</div>


</div>

<!-- Modal Crear Viaje-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width: 450px">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo Viaje</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form>
      <div class="form-group">
    <label for="exampleInputPassword1">Dale un Nombre</label>
    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nombre del viaje">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Descripcion</label>
    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Ingresa una descripcion">
  </div>
  <div class="form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Selecciona para que sea privado</label>
  </div>
</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#invitar">Crear</button>

      </div>
    </div>
  </div>
</div>

<!-- Modal Invitar y Compartir-->
<div class="modal fade" id="modalinvitar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width: 450px">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Invitar a este Itinerario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">...</div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>